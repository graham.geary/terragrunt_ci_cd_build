FROM hashicorp/terraform:0.11.13
RUN apk add libc6-compat
RUN apk add wget
RUN wget https://github.com/gruntwork-io/terragrunt/releases/download/v0.18.3/terragrunt_linux_amd64
RUN mv -v terragrunt_linux_amd64 /bin/terragrunt
RUN chmod +x /bin/terragrunt
ENTRYPOINT ["/bin/terragrunt"]